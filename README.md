# Post Type Echo Plugin

* Plugin Name: Post Type Echo
* Description: Echo any Post Type by widget
* Version: 1.0

## Installation

* From Admin page go to Plugins / Add New / Upload Plugin
* Choose .zip file with plugin
* Install now
* Enjoy your new widget