<?php
/**
 * Widget API: PT_Echo_Widget class
 *
 * @package PT_Echo
 */

/**
 * Class PT_Echo_Widget
 */
class PT_Echo_Widget extends WP_Widget {

	function __construct() {
		parent::__construct( 'pt_echo_widget', __( 'Post Type Widget', 'pt_echo' ),
			array( 'description' => __( 'Show post from any post type', 'pt_echo' ), )
		);
	}


	/**
	 *
	 * Update Widgets parameters
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		//TODO: use merge
		$instance = array();

		$instance['title']      = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['post_type']  = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : 'post';
		$instance['order_by']   = ( ! empty( $new_instance['order_by'] ) ) ? strip_tags( $new_instance['order_by'] ) : 'date';
		$instance['sort']       = ( ! empty( $new_instance['sort'] ) ) ? strip_tags( $new_instance['sort'] ) : 'ASC';
		$instance['post_count'] = ( ! empty( $new_instance['post_count'] ) ) ? strip_tags( $new_instance['post_count'] ) : 3;
		$instance['columns']    = ( ! empty( $new_instance['columns'] ) ) ? strip_tags( $new_instance['columns'] ) : 3;
		$instance['view_more']  = ( ! empty( $new_instance['view_more'] ) ) ? $new_instance['view_more'] : '';

		return $instance;

	}

	/**
	 *
	 * Show Widget form in Widget Admin menu
	 *
	 * @param $instance
	 */

	public function form( $instance ) {

		//TODO: use merge
		$title      = isset( $instance['title'] ) ? $instance['title'] : '';
		$post_type  = isset( $instance['post_type'] ) ? $instance['post_type'] : 'post';
		$order_by   = isset( $instance['order_by'] ) ? $instance['order_by'] : 'date';
		$sort       = isset( $instance['sort'] ) ? $instance['sort'] : 'ASK';
		$post_count = isset( $instance['post_count'] ) ? $instance['post_count'] : 3;
		$columns    = isset( $instance['columns'] ) ? $instance['columns'] : 3;
		$view_more  = isset( $instance['view_more'] ) ? $instance['view_more'] : false;

		// TODO: should be template
		?>
		<p>
			<label><?php _e( 'Title:', 'pt_echo' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
				       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
				       value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>

		<?php
		$post_types = get_post_types( array(
			'public' => true
		) );
		?>
		<p>
			<label><?php _e( 'Post Type:', 'pt_echo' ); ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'post_type' ); ?>"
				        id="<?php echo $this->get_field_id( 'post_type' ); ?>">
					<?php foreach ( $post_types as $type ) :
						$selected = $type === $post_type ? 'selected' : '';
						?>
						<option value="<?php echo $type ?>" <?php echo $selected ?>><?php echo $type ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</p>
		<?php
		$order_by_arr = array( 'date', 'title', 'rand' );
		?>
		<p>
			<label><?php _e( 'Sort By:', 'pt_echo' ); ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'order_by' ); ?>"
				        id="<?php echo $this->get_field_id( 'order_by' ); ?>">
					<?php foreach ( $order_by_arr as $type ) :
						$selected = $type === $order_by ? 'selected' : '';
						?>
						<option value="<?php echo $type ?>" <?php echo $selected ?>><?php echo $type ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</p>


		<?php
		$sort_arr = array( 'ASC', 'DESC' );
		?>
		<p>
			<label><?php _e( 'Sort:', 'pt_echo' ); ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'sort' ); ?>"
				        id="<?php echo $this->get_field_id( 'sort' ); ?>">
					<?php foreach ( $sort_arr as $type ) :
						$selected = $type === $sort ? 'selected' : '';
						?>
						<option value="<?php echo $type ?>" <?php echo $selected ?>><?php echo $type ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</p>

		<p>
			<label><?php _e( 'Post count:', 'pt_echo' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'post_count' ); ?>"
				       name="<?php echo $this->get_field_name( 'post_count' ); ?>" type="text"
				       value="<?php echo esc_attr( $post_count ); ?>"/>
			</label>
		</p>
		<?php
		$columns_arr = array( 1, 2, 3, 4, 6, 12 );
		?>
		<p>
			<label><?php _e( 'Columns:', 'pt_echo' ); ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'columns' ); ?>"
				        id="<?php echo $this->get_field_id( 'columns' ); ?>">
					<?php foreach ( $columns_arr as $type ) :
						$selected = $type == $columns ? 'selected' : '';
						?>
						<option value="<?php echo $type ?>" <?php echo $selected ?>><?php echo $type ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</p>

		<p>
			<label><?php _e( 'View more link:', 'pt_echo' ); ?>
				<input class="widefat" id="<?php echo $this->get_field_id( 'view_more' ); ?>"
				       name="<?php echo $this->get_field_name( 'view_more' ); ?>" type="checkbox"
				       value="<?php echo 1; ?>" <?php echo $view_more ? 'checked' : '' ?>/>
			</label>
		</p>
		<?php
	}


	/**
	 *
	 * Echo Posts
	 *
	 * @param array $args
	 * @param array $instance
	 */

	public function widget( $args, $instance ) {

		pt_eco_get_template_part( 'pt_echo_list',
			array(
				'instance' => $instance,
				'args' => $args
			) );

	}
}

