<?php
/**
 * Plugin Name: Post Type Echo
 * Description: Echo any Post Type by widget
 * Version: 1.0
 * Author: Unnamed
 *
 * @package PT_Echo
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'PTEcho' ) ) :

	/**
	 * Main Post Type Echo Class.
	 *
	 * @class PTEcho
	 * @version    1.0.0
	 */

	class PTEcho {
		/**
		 * The single instance of the class.
		 *
		 * @var PTEcho
		 */
		protected static $_instance = null;


		/**
		 * Main PTEcho Instance.
		 *
		 *
		 * @static
		 * @see PTEcho()
		 * @return PTEcho - Main instance.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}


		/**
		 * PTEcho Constructor.
		 */
		public function __construct() {
			$this->define_constants();
			$this->includes();
			$this->init_hooks();

			do_action( 'pt_echo_loaded' );
		}

		/**
		 * Hook into actions and filters.
		 */
		private function init_hooks() {
			register_activation_hook( __FILE__, 'activate' );
			register_deactivation_hook( __FILE__, 'deactivate' );

			add_action( 'widgets_init', array( $this, 'add_pt_widget' ) );

			add_action('wp_enqueue_scripts', array($this, 'register_pt_styles'));
		}

		/**
		 * Define Constants.
		 */
		private function define_constants() {
			$this->define( 'PT_ECHO_DIR', plugin_dir_path( __FILE__ ) );
			$this->define( 'PT_ECHO_URL', plugin_dir_url( __FILE__ ) );
		}

		/**
		 * Define constant if not already set.
		 *
		 * @param  string $name
		 * @param  string|bool $value
		 */
		private function define( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * Include required core files.
		 */
		public function includes() {
			include_once( PT_ECHO_DIR . 'includes/pt_echo_core_functions.php' );
			include_once( PT_ECHO_DIR . 'includes/pt_echo_widget.php' );
		}

		/**
		 * Register styles
		 */
		function register_pt_styles() {
			wp_enqueue_style('pt-echo-style', PT_ECHO_URL . 'assets/css/pt_echo_styles.css', array(), '1.0.1');
		}

		/**
		 * Activate plugin function.
		 */
		public static function activate() {

		}


		/**
		 * Deactivate plugin function.
		 */
		public static function deactivate() {

		}

		/**
		 * Widget register function
		 */
		function add_pt_widget() {
			register_widget( 'pt_echo_widget' );
		}

	}

endif;

/**
 * Main instance of PTEcho.
 *
 * Returns the main instance of PTEcho to prevent the need to use globals.
 *
 * @return PTEcho
 */
function PTEco() {
	return PTEcho::instance();
}

$GLOBALS['pt_echo'] = PTEco();