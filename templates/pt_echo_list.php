<?php
/**
 * @var $instance ;
 * @var $args ;
 */
// TODO: Это секция логики не надо ее пхать в темплейт
// TODO: ф-ции compact/extract
$title      = apply_filters( 'pt_eidget_title', $instance['title'] );
$post_type  = apply_filters( 'pt_eidget_post_type', $instance['post_type'] );
$order_by   = apply_filters( 'pt_eidget_order_by', $instance['order_by'] );
$sort       = apply_filters( 'pt_eidget_sort', $instance['sort'] );
$post_count = apply_filters( 'pt_eidget_post_count', $instance['post_count'] );
$columns    = apply_filters( 'pt_eidget_columns', $instance['columns'] );
$view_more  = apply_filters( 'pt_eidget_view_more', $instance['view_more'] );


// before and after widget arguments are defined by themes
echo $args['before_widget'];

if ( ! empty( $title ) ) {
	echo $args['before_title'] . $title . $args['after_title'];
}

$columns_final = intval( 12 / (int) $columns );
$columns_class = 'pt-eco-item pt-eco-col-' . $columns_final . ' pt-eco-' . $post_type;
if ( $columns == 1 ) {
	$columns_class .= ' pt-eco-item--full';
}
$posts = new WP_Query( array(
	'post_type'      => $post_type,
	'orderby'        => $order_by,
	'order'          => $sort,
	'posts_per_page' => (int) $post_count
) );

$columns_class = apply_filters('pt_echo_column_class', $columns_class);
$view_more = apply_filters('pt_echo_view_more_link', $view_more);

// TODO: это темплейт остальное надо убрать отседа
if ( $posts->have_posts() ) : ?>
	<div class="pt-eco-list">
		<?php
		while ( $posts->have_posts() ): $posts->the_post();
			pt_eco_get_template_part( 'pt_echo_list_item',
				array(
					'columns_class' => $columns_class,
					'view_more'     => $view_more
				) );
		endwhile;
		?>

	</div>
<?php endif;

wp_reset_query();

echo $args['after_widget'];
